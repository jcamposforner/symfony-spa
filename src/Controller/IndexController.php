<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{

    /**
     * @Route("/", requirements={"index"="^(?!api|_(profiler|wdt)).*"}, name="index")
     * @return Response
     */
    public function index()
    {
        return $this->render('base.html.twig');
    }

    /**
     * @Route("/{vueRoutes}", requirements={"vueRouting"="^(?!api|_(profiler|wdt)).*"}, name="vueRoutes")
     * @return Response
     */
    public function vueRoutes()
    {
        return $this->render('base.html.twig');
    }
}
